export interface Cotizacion {
	base : string;
	date : string;
	rates : { [EUR : string] : string}
}

export interface CoinModel {
	shortName:string;
	symbol:string;
	value : string;
}