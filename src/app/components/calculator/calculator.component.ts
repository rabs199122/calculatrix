import { Component, OnInit, Input } from '@angular/core';
import { CotizacionService } from 'src/app/services/cotizacion.service';
import { CoinModel } from 'src/app/models/cotizacion';
import { interval } from 'rxjs';
import { flatMap } from 'rxjs/operators';



@Component({
	selector: 'calculatrix',
  	templateUrl: './calculator.component.html',
  	styleUrls: ['./calculator.component.scss']
})

export class CalculatorComponent implements OnInit {
  
	firstValue : CoinModel;
	secondValue : CoinModel;
	usTempValue: string;

	@Input() firstCoin: CoinModel;
	@Input() secondCoin : CoinModel;

	constructor(public cotizacionService : CotizacionService){};
	

	formatValue(value, symbol): void {

		let isValid: boolean = this.validInput(this[value].value, symbol);

		if (isValid) {
			this[value].value = this.formatCoins(this[value].value, symbol);
			this.usTempValue = this[value].value;
		} else {
			this[value].value = this.usTempValue;
		};

	};

	validInput(value, symbol): boolean {

		let points = 0;
		let regex = new RegExp('^[0-9,.' + symbol + ']+$', 'g');
		let res = value.match(regex) != null;

		for (let i in value) {
			if (value[i] === '.'){
				points++;
			};
		};

		if (value === "") {
			return true;
		};

		if (points > 1) {
			return false;
		};

		return res;

	};

	getComputableVal(value, symbol): number {

		let computableVal;
		let regex = new RegExp('[,'+symbol+']', 'g');

		if (value) {
			computableVal = Number(parseFloat(value.replace(regex, '')).toFixed(4));
		} else {
			this.firstValue.value = this.firstValue.symbol + '0'; 
			computableVal = 0;
		};

		return computableVal;

	};

	formatCoins(value, symbol): string {
		
		let formatedNumber: string;
		let finalValue: string;
		var regex = new RegExp('[,'+symbol+']', 'g');
		formatedNumber = value.replace(regex, '');
		finalValue = symbol + this.numberWithCommas(formatedNumber);

		return finalValue;
		
	};

	numberWithCommas(number) {

		var parts = number.toString().split(".");
		
		if (parts[1] && parts[1].length > 4) {
			parts[1] = parts[1].slice(0,4);
		};
		
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");

	};

	makeCotizacion(firstVal : CoinModel, secondVal : string) : string {

		if(firstVal.value === firstVal.symbol) {
			firstVal.value+= '0';
		};

		let parseFirstVal: number = this.getComputableVal(firstVal.value, firstVal.symbol);
		let parseSecondVal = Number(secondVal);
		let cotizacion = (parseFirstVal * parseSecondVal).toFixed(4);
	
		return cotizacion;

	};

	convert() {

		let cotizacion: string;
		this.cotizacionService.getEur().subscribe(data => {
			cotizacion = this.makeCotizacion(this.firstValue, data.rates[this.secondValue.shortName]);
			this.secondValue.value = this.secondValue.symbol + this.numberWithCommas(cotizacion);
			interval(10 * 60 * 1000).pipe(
				flatMap(() => this.cotizacionService.getEur())
				).subscribe(data => {
				cotizacion = this.makeCotizacion(this.firstValue, data.rates[this.secondValue.shortName]);
				this.secondValue.value = this.secondValue.symbol + this.numberWithCommas(cotizacion);
			});
		});

	};

	ngOnInit() {
	
		this.firstValue = this.firstCoin || { shortName: 'US', symbol: '$' , value:''};
		this.firstValue.value = this.firstValue.symbol + '0';
		this.secondValue = this.secondCoin ||  { shortName: 'EUR', symbol:'€', value:''};
		this.secondValue.value = this.secondValue.symbol + '0';

	};

};
