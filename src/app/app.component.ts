import { Component } from '@angular/core';
import { CoinModel } from './models/cotizacion';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	firstCoin: CoinModel = { shortName: 'USD', symbol: "$", value:''};
	secondCoin: CoinModel = { shortName: 'EUR', symbol:"€", value:''};
	
};
