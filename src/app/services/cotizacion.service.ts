import { Injectable } from '@angular/core';
import { Cotizacion } from '../models/cotizacion'

import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class CotizacionService {

	private getRandomNumber(): string{

		let random = (Math.random() * (2.0000 - 0.0000) + 0.0000).toFixed(4);
		return random;

	};

	getEur(): Observable<Cotizacion>{

		return new Observable(observer => {
			
			let eurCotizacion = {
				"base" : "USD",
				"date" : "2017-06-11",
				"rates" : {
					"EUR": this.getRandomNumber(),
					"CHI": this.getRandomNumber(),
					"PE": this.getRandomNumber()
				}
			}

			observer.next(eurCotizacion);
			observer.complete();
			
			return () => console.log('emitido')
		});

	};
};


