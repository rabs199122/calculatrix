#IMPORTANTE
por alguna razon de la plataforma que me ofrece la api, no pude accder a ella, de igual forma simule una especie de api/consulta
improvisada para cumplir con el objetivo del ejercicio, por otra parte, tampoco se aplica test unitario.
# BellatrixCalculator

Calculador de divisas realizado con el [Angular CLI](https://github.com/angular/angular-cli) version 6.1.4.

## Development server

ejecuta `ng serve` para el servidor de desarrollo y navega en tu explorador a `http://localhost:4200/`. 

## Componentes
###header
componente con la barra de navegacion

###calculatrix
componente que realiza el calculo de divisas, tiene dos parametros :


-firstCoin implementa la interfaz CoinModel, es la moneda que se desea averiguar su cambio.
valor por defecto:  { shortName: 'US', symbol: '$' , value:''}, donde shortName es la abreviacion de la moneda, symbol, el simbolo que se desea ver en el input, y value, el valor que manejara en su implementacion, SIEMPRE este por defecto al iniciar es 0.


-secondCoin implementa la interfaz CoinModel, es la modela en base a la cual se va a realizar el cambio.
valor por defecto: { shortName: 'EUR', symbol:'�', value:''}, donde shortName es la abreviacion de la moneda, symbol, el simbolo que se desea ver en el input, y value, el valor que manejara en su implementacion, SIEMPRE este por defecto al iniciar es 0.

###footer
componente que muestra el footer de la pagina.





